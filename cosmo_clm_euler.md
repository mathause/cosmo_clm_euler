# Setup COSMO-CLM² on euler

**NOTE**: certain instruction must be done every time (e.g. exporting the PROJECT folder), some only once


## Startup

### Add ssh key for github

Required to access ``cosmo@c2sm-features``

### Define the 'project' folder


**IMPORTANT**: using fast project folder where Sonia has not purchased any storage. Make sure not to write (large) input and output there!

* create project folder (once)
   ```bash
   mkdir -p /cluster/project/climate/$USER/cosmo_clm2_euler
   ```
* export `PROJECT` folder
  ```bash
  # export required for compile_cclm2_euler.sh
  export PROJECT=/cluster/project/climate/$USER/cosmo_clm2_euler
  ```

## Spack

### "Install" spack

* download spack-c2sm

  ```bash

  cd $PROJECT

  git clone https://github.com/C2SM/spack-c2sm.git

  cd spack-c2sm/
  git submodule update --init --depth 1

  ```

* spack config ($\rightarrow$ **not working**)

  ```bash
  cat .spack/config.yaml
  ```

  ```yaml
  config:
    install_tree:
      root: /cluster/work/climate/mathause/cosmo_clm2_euler/spack-install/
  ```
  Packages are installed to `$PROJECT/spack-c2sm/spack/opt/spack/...`


### initialize spack-c2sm

```bash

source $PROJECT/spack-c2sm/setup-env.sh

```

## OASIS

### Spack install OASIS for CLM-side (gcc)

```bash
spack install oasis@4.0%gcc@8.5.0 +fix_mct_conflict
```


### Spack install OASIS for COSMO-side (~nvhpc~)

-> not on euler as this is nvhpc is the gpu compiler

```bash
# must be done differently -> nvhpc is the nvidia compiler
# spack install oasis@master%nvhpc +fix_mct_conflict
```


## CLM

### Sync domain and surfdata data

```bash
rsync -avvH exo:/landclim/pesieber/data/CCLM2_EUR_inputdata CCLM2_inputdata
```

**NOTE**: `OASIS_dummy_for_datm` data must be copied from daint (is not on exo)


### Clone .cime to ~/.cime (for modules, compiler links and flags)

-> I THINK THAT IS NOT NECESSARY AS THESE ARE THE DAINT SETTINGS -> WE USE THE SETTINGS FROM URS

```bash

cd $PROJECT
git clone git@github.com:pesieber/.cime

```

### Clone the OASIS interface for CLM in $PROJECT/CCLM2

```bash
cd $PROJECT
git clone https://github.com/pesieber/cesm2_oasis.git
```

### Clone CTSM in $PROJECT/CCLM2 and check out externals

```bash
cd $PROJECT
git clone -b release-clm5.0 clm5.0

cd clm5.0
./manage_externals/checkout_externals
```

### Add configuration

Use combined config from Urs and Petra (see cesm_config folder)

**CESM212 - new software stack**

```bash
pushd $PROJECT/clm5.0/cime/config/cesm

BASE_URL=https://git.iac.ethz.ch/mathause/cosmo_clm_euler/-/raw/main/cesm_config/cesm2.1.2

wget ${BASE_URL}/config_inputdata.xml -O config_inputdata.xml

pushd machines
wget ${BASE_URL}/machines/config_batch.xml     -O config_batch.xml
wget ${BASE_URL}/machines/config_machines.xml  -O config_machines.xml
wget ${BASE_URL}/machines/config_compilers.xml -O config_compilers.xml

popd; popd;
```

Adapt cesm2.2.2/XML/env_mach_specific.py

```diff
-source_cmd = "source {} && ".format(init_path)
+source_cmd = ". {} && ".format(init_path)
```

### Complie cclm2 euler

```bash
cd $PROJECT/clm5

module load stack/.2024-04-silent

module load python
module load libxml2/2.10.3-5uc2w2w

./compile_cclm2_euler.sh
```


## COSMO

### Clone cosmo to the COSMO_CLM2 branch

```bash
cd $PROJECT
git clone -b COSMO_CLM2 https://github.com/C2SM-RCM/cosmo.git cosmo_cclm2
```

### Compile COSMO with OASIS support

1. Make sure you are on the `COSMO_CLM2` branch (`git branch`)
   ```bash
   cd $PROJECT/cosmo_cclm2
   git branch
   ```
2. Only when re-compiling: Make sure you have a clean state
   ```bash
   cd $PROJECT/cosmo_cclm2/cosmo/ACC
   export F90=dummy; make clean
   ```
3. Compile
   ```bash
   cd $PROJECT/cosmo_cclm2
   spack dev-build cosmo@c2sm-features%gcc@8.5.0 +oasis ^oasis
   ```

### INT2LM
Spack install int2lm
```bash
spack install int2lm@c2sm-features%gcc@8.5.0
```


### EXTPAR

Skipped











