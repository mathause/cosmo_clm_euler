# CESM config

Uses the configuration for CESM 2.2.2 for the new euler OS by Urs Beyerle, adapts it to CESM 2.1.2 (i.e. the used version via CTSM), and adds the oasis-spefific files.


## Copy original files


```bash
mkdir -p cesm2.1.2/cd machines

pushd cesm2.1.2/

BASE_URL="https://git.iac.ethz.ch/cesm2/config/-/raw/main"

wget ${BASE_URL}/cesm2.2.2/config_inputdata.xml -O config_inputdata.xml

pushd machines
wget ${BASE_URL}/cesm2.2.2/machines/config_batch.xml     -O config_batch.xml
wget ${BASE_URL}/cesm2.2.2/machines/config_machines.xml  -O config_machines.xml
# NOTE: config_compilers.xml from 2.2.1
wget ${BASE_URL}/cesm2.1.2/machines/config_compilers.xml -O config_compilers.xml

popd; popd
```

For necessary changes: see git history
